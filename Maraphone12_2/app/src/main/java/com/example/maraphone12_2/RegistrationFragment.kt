package com.example.maraphone12_2

import android.app.ActionBar
import android.content.ContentValues.TAG
import android.os.Bundle
import android.text.Layout
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment.Companion.findNavController
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class RegistrationFragment : Fragment() {
    lateinit var auth: FirebaseAuth
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        auth = Firebase.auth
        // Inflate the layout for this fragment
        //activity?.findViewById<BottomNavigationView>(R.id.bottomNavView)?.visibility = View.GONE
        val view = inflater.inflate(R.layout.fragment_registration, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch() {
            sign(view)
        }
        /*
        val waitIm = ImageView(context)
        waitIm.setImageResource(R.drawable.imgwait)
        val lytParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        waitIm.layoutParams = lytParams
        view.findViewById<ConstraintLayout>(R.id.regConstr).addView(waitIm)
    */
        val currentUser = auth.currentUser
        if(currentUser != null) {
            //Navigation.findNavController(view).navigate(R.id.action_registrationFragment_to_loginFragment2)
            updateUI(currentUser, view)
            //currentUser.reload()
        }
        //findNavController(view).navigate(R.id.action_registrationFragment_to_loginFragment2)
        view.findViewById<Button>(R.id.button2).setOnClickListener {
            lifecycleScope.launch {
                accCreating(view.findViewById<EditText>(R.id.editTextTextEmailAddress).text.toString(), view.findViewById<EditText>(R.id.editTextTextPassword).text.toString(), view)
            }
        }
        view.findViewById<Button>(R.id.btnToLogin).setOnClickListener {
            lifecycleScope.launch {
                sign(view)
            }
        }

    }

    suspend fun sign(view: View) {
        if(view.findViewById<EditText>(R.id.editTextTextEmailAddress).text.toString() != "" && view.findViewById<EditText>(R.id.editTextTextPassword).text.toString()!="")
            signIn(view.findViewById<EditText>(R.id.editTextTextEmailAddress).text.toString(), view.findViewById<EditText>(R.id.editTextTextPassword).text.toString(), view)
    }

    suspend fun accCreating(email: String, password: String, view: View) {
        if(view.findViewById<EditText>(R.id.editTextTextEmailAddress).text.toString() != "" && view.findViewById<EditText>(R.id.editTextTextPassword).text.toString()!="")
            createAccount(view.findViewById<EditText>(R.id.editTextTextEmailAddress).text.toString(), view.findViewById<EditText>(R.id.editTextTextPassword).text.toString(), view)
    }





    fun createAccount(email: String, password: String, view: View) {
        // [START create_user_with_email]
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "createUserWithEmail:success")
                    val user = auth.currentUser
                    updateUI(user, view)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "createUserWithEmail:failure", task.exception)
                    Toast.makeText(
                        this.context, "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                    //updateUI(null, view)
                }
            }
        // [END create_user_with_email]
    }

    private fun updateUI(user: FirebaseUser?, view: View) {
        Navigation.findNavController(view).navigate(R.id.action_registrationFragment_to_loginFragment2)
       // Navigation.findNavController().navigate(R.id.action_registrationFragment_to_loginFragment2)
    }

    suspend fun signIn(email: String, password: String, view: View) {
        // [START sign_in_with_email]
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithEmail:success")
                    val user = auth.currentUser
                    updateUI(user, view)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithEmail:failure", task.exception)
                    Toast.makeText(
                        this.context, "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                    //updateUI(null, view)
                }
            }
        delay(400L)
    }


    companion object {

        fun newInstance(param1: String, param2: String) =
            RegistrationFragment()
    }
}



