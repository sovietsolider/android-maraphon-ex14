package com.example.maraphone12_2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.maraphone12_2.databinding.RecItemBinding

class RecyclerAdapter: RecyclerView.Adapter<RecyclerAdapter.RecHolder>() {
    val itemsList = ArrayList<RecItem>()
    class RecHolder(item: View): RecyclerView.ViewHolder(item) {
        val binding = RecItemBinding.bind(item)
        fun bind(itm: RecItem) {
            binding.textView.text = itm.data
        }
    }

    fun addItem(itm: RecItem) {
        itemsList.add(itm)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.rec_item, parent, false)
        return RecHolder(view)
    }

    override fun onBindViewHolder(holder: RecHolder, position: Int) {
        holder.bind(itemsList[position])
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }
}