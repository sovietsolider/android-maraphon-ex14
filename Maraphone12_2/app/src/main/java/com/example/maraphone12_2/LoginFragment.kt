package com.example.maraphone12_2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.Navigation
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class LoginFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_login, container, false)
        view.findViewById<Button>(R.id.btnToHome).setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_homeFragment)
        }
        view.findViewById<Button>(R.id.btnToReg).setOnClickListener {
            Firebase.auth.signOut()
            Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_registrationFragment)
        }
        view.findViewById<TextView>(R.id.textViewLogin).text = Firebase.auth.currentUser?.email


        return view
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            LoginFragment()
    }
}